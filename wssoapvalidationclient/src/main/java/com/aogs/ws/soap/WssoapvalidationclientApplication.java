package com.aogs.ws.soap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WssoapvalidationclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(WssoapvalidationclientApplication.class, args);
	}

}
