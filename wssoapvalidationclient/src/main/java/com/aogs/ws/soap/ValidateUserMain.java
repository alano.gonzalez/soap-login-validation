package com.aogs.ws.soap;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import com.aogs.ws.trainings.ValidatePortType;
import com.aogs.ws.trainings.ValidateRequest;
import com.aogs.ws.trainings.ValidateResponse;

public class ValidateUserMain {

	public static void main(String[] args) throws MalformedURLException {
		
		ValidateUsersWsImplService service = new ValidateUsersWsImplService(new URL("http://localhost:8080/wssoapvalidation/validateuser?WSDL"));
		
		ValidatePortType validateUsersWsImplPort = service.getValidateUsersWsImplPort();
		
		ValidateRequest request = new ValidateRequest();
		
		Scanner reader = new Scanner(System.in);
		String id, verify;
		boolean valid = false;
		
		while(!valid) {
			System.out.println("id: ");
			id = reader.nextLine();
			System.out.println("VerifyCode: ");
			verify = reader.nextLine();
			
			valid = validate(id, verify, validateUsersWsImplPort);
			if(valid) {
				System.out.println("Login Correcto");
			}
			else {
				System.out.println("Login incorrecto");
			}
		}
		
	}
	
	public static boolean validate(String id, String verify, ValidatePortType validatewport) {
		ValidateRequest request = new ValidateRequest();
		request.setId(new BigInteger(id));
		request.setVerifycode(verify);
		ValidateResponse response = validatewport.validateUser(request);
		return response.isResult();
	}
	
}
