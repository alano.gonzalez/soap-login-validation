package com.aogs.ws.soap.service;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.aogs.ws.trainings.User;

public class ReadRegisters {
	
	public static List<User> getAllUsers(){
		List<User> users = new ArrayList();
		try {
			FileInputStream file = new FileInputStream(new File("RegistrosMomentum.xls"));
			
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			HSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			Row row;
			rowIterator.next();
			Map<Integer, User> mapa = new HashMap<>();
			while (rowIterator.hasNext()) {
				row = rowIterator.next();
				User u = new User();
				u.setId(new BigInteger(row.getCell(3).getStringCellValue()));
				u.setVerifycode(row.getCell(5).getStringCellValue());
				try {
					mapa.put(u.getId().intValue(), u);
				}catch(Exception e) {
				}
			}
			for(Entry<Integer, User> entry : mapa.entrySet()) {
			    User value = entry.getValue();
			    users.add(value);
			    System.out.println(value);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	
}
