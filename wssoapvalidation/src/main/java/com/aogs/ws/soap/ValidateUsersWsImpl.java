package com.aogs.ws.soap;

import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.feature.Features;

import com.aogs.ws.soap.service.ReadRegisters;
import com.aogs.ws.trainings.User;
import com.aogs.ws.trainings.ValidatePortType;
import com.aogs.ws.trainings.ValidateRequest;
import com.aogs.ws.trainings.ValidateResponse;


@Features(features="org.apache.cxf.feature.LoggingFeature")
public class ValidateUsersWsImpl implements ValidatePortType{
	
	 
    List<User> users = new ArrayList<>();
    
    public ValidateUsersWsImpl() {
    	init();
    }

    public void init() {
    	users = ReadRegisters.getAllUsers();
    }

	@Override
	public ValidateResponse validateUser(ValidateRequest request) {
		int id = request.getId().intValue();
		String verifycode = request.getVerifycode();
		ValidateResponse response = new ValidateResponse();
		for(User user:users) {
			if(user.getId().intValue() == id && user.getVerifycode().equals(verifycode)) {
				response.setResult(true);
				return response;
			}
		}
		
		response.setResult(false);
		return response;
	}
    
}
