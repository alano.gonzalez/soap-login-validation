package com.aogs.ws.soap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WssoapvalidationApplication {

	public static void main(String[] args) {
		SpringApplication.run(WssoapvalidationApplication.class, args);
	}

}
